const router = require('express').Router();
const User = require('../models/User');

//Get user list
router.post('/', async (req, res) => {
	try {
		const users = await User.find({});
		res.status(200).send(users);
	} catch (err) {
		res.status(500).send(err);
	}
});
//Single user by id
router.post('/:id', async (req, res) => {
	try {
		const user = await User.findOne({ _id: req.params.id });
		res.status(200).send(user);
	} catch (err) {
		res.status(500).send(err);
	}
});
router.patch('/:id', async (req, res) => {
	try {
	} catch (err) {
		res.status(400).send(err);
	}
});
module.exports = router;
