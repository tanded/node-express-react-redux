const router = require('express').Router();
const User = require('../models/User');
const { registerValidation, loginValidation } = require('../validation/user.validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
//Register
router.post('/register', async (req, res) => {
	//Validate data
	const { error } = registerValidation(req.body);
	if (error) return res.status(400).send(error);
	//Check if user exists
	const userExists = await User.findOne({ email: req.body.email });
	if (userExists) return res.status(400).send('User exists');
	//Hash password
	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash(req.body.password, salt);
	//Create user
	const user = new User({
		name: req.body.name,
		email: req.body.email.toLowerCase(),
		password: hashedPassword
	});
	try {
		const newUser = await user.save();
		res.send(newUser);
	} catch (err) {
		res.status(400).send(err);
	}
});
//Login
router.post('/login', async (req, res) => {
	//Validate data
	const { error } = loginValidation(req.body);
	if (error) return res.status(400).send(error);
	//Check if user exists
	const user = await User.findOne({ email: req.body.email });
	if (!user) return res.status(400).send('User with such email doesnt exists');
	//Password is correct
	const validPassword = await bcrypt.compare(req.body.password, user.password);
	if (!validPassword) return res.status(400).send('Invalid password');
	//Create token
	const token = jwt.sign({ user }, process.env.TOKEN_SECRET);
	const response = {
		token,
		user: {
			user: user.name,
			email: user.email
		}
	};
	res.header('auth-token', token).send(response);
});
router.post('/data', async (req, res) => {
	//if (!user) return res.status(400).send('Invalid token');
	try {
		const { user } = await jwt.verify(req.body.token, process.env.TOKEN_SECRET);
		const response = {
			_id: user._id,
			name: user.name,
			email: user.email,
			data: user.date
		};
		res.status(200).send(response);
	} catch (err) {
		res.status(400).send(err);
	}
});
module.exports = router;
