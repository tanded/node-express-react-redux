const Joi = require('@hapi/joi');

const registerValidation = (user) => {
	const schema = Joi.object({
		name: Joi.string().min(6).required(),
		email: Joi.string().min(6).max(255).email().required(),
		password: Joi.string().min(6).max(1024).required()
	});
	return schema.validate(user);
};
const loginValidation = (user) => {
	const schema = Joi.object({
		email: Joi.string().min(6).max(255).email().required(),
		password: Joi.string().min(6).max(1024).required()
	});
	return schema.validate(user);
};
module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
