const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const path = require('path');
const webpackConfig = require('../webpack.config.dev');

//Routes
const authRoute = require('./routes/auth');
const usersRoute = require('./routes/users');
dotenv.config();

//Connect Database
mongoose.connect(process.env.DATABASE_CONNECT, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
	console.log('Database connected');
});
//Middleware
app.use(express.json());
//Dev Middleware
const compiler = webpack(webpackConfig);
app.use(
	webpackMiddleware(compiler, {
		hot: true,
		publicPath: webpackConfig.output.publicPath,
		noInfo: true
	})
);
app.use(webpackHotMiddleware(compiler));
//Routes Api
app.use('/api/user', authRoute);
app.use('/api/users', usersRoute);
//Route App
app.get('/*', (req, res) => {
	res.sendFile(path.join(__dirname, './index.html'));
});

app.listen(process.env.PORT, () => console.log('Server is running on port ' + process.env.PORT));
