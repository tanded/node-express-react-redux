const path = require('path');
const webpack = require('webpack');

module.exports = {
	devtool: 'eval-source-map',
	mode: 'development',
	entry: [ 'webpack-hot-middleware/client?reload=true', path.join(__dirname, './client/index.js') ],
	output: {
		path: '/',
		publicPath: '/'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.optimize.OccurrenceOrderPlugin()
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				include: path.join(__dirname, 'client'),
				use: {
					loader: 'babel-loader',
					options: {
						presets: [ '@babel/preset-env' ]
					}
				}
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					// Creates `style` nodes from JS strings
					'style-loader',
					// Translates CSS into CommonJS
					'css-loader',
					// Compiles Sass to CSS
					'sass-loader'
				]
			}
		]
	},
	resolve: {
		extensions: [ '.js' ]
	}
};
