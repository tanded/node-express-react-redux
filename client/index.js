import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import { getByToken } from './store/actions/auth.actions';
import App from './App';

if (localStorage.token) {
	store.dispatch(getByToken(localStorage.token));
}

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('app')
);
