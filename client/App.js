import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import history from './utils/histrory';
import { Homepage } from './components/Homepage';
import { LoginPage } from './components/LoginPage';
import { RegisterPage } from './components/RegisterPage';
import { Navigation } from './components/Navigation';
import { ProfilePage } from './components/ProfilePage';
class App extends Component {
	render() {
		return (
			<div className='container'>
				<Router history={history}>
					<Navigation />
					<Switch>
						<Route exact path='/' component={Homepage} />
						<Route path='/login' component={LoginPage} />
						<Route path='/register' component={RegisterPage} />
						<Route path='/profile' component={ProfilePage} />
					</Switch>
				</Router>
			</div>
		);
	}
}
export default App;
