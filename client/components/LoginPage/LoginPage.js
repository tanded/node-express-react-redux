import React, { Component, useEffect, useState } from 'react';
import './LoginPage.scss';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { login } from '../../store/actions/auth.actions';
import history from '../../utils/histrory';

const LoginPage = (props) => {
	const [ email, setEmail ] = useState('test@gmail.com');
	const [ password, setPassword ] = useState('option123');

	useEffect(() => {
		if (props.isAuthenticated) {
			history.push('/');
		}
	});
	const checkForm = (e) => {
		e.preventDefault();
		const credits = {
			email,
			password
		};
		props.sendForm(credits);
	};
	return (
		<div className='login'>
			<input name='email' type='email' placeholder='email' value={email} onChange={(e) => setEmail(e.target.value)} />
			<input
				name='password'
				type='password'
				placeholder='password'
				value={password}
				onChange={(e) => setPassword(e.target.value)}
			/>
			<button onClick={(e) => checkForm(e)}>Login</button>
			<Link to='/register'>Dont have an acount yet?</Link>
		</div>
	);
};
const mapStateToProps = (state) => {
	const { isAuthenticated } = state;
	return {
		isAuthenticated
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		sendForm: (credits) => {
			dispatch(login(credits));
		}
	};
};

const connected = connect(mapStateToProps, mapDispatchToProps)(LoginPage);

export { connected as LoginPage };
