import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './Navigation.scss';
const Navigation = (props) => {

	const links = ( auth ) =>{
		if(auth){
			return(
				<>
					<Link to='/'>Homepage</Link>
					<Link to='/profile'>Profle</Link>
				</>
			)
		}else{
			return(
				<>
					<Link to='/'>Homepage</Link>
					<Link to='/login'>Login</Link>
					<Link to='/register'>Register</Link>
				</>
			)
		}
	}
	console.log(props)
	return (
		<nav className='nav'>
			<div className='container'>
				{links(props.isAuthenticated)}
			</div>
		</nav>
	);
};
const mapStateToProps = (state) => {
	const { isAuthenticated } = state;
	return {
		isAuthenticated
	};
};
const connected = connect(mapStateToProps)(Navigation);

export { connected as Navigation };
