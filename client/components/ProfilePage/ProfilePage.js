import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
const ProfilePage = (props) => {
	console.log(props);
	return (
		<nav className='nav'>
			<div className='container'>
				<div>Name : {props.user.name}</div>
				<div>Email : {props.user.email}</div>
			</div>
		</nav>
	);
};
const mapStateToProps = (state) => {
	const { user } = state;
	return {
		user
	};
};
const connected = connect(mapStateToProps)(ProfilePage);

export { connected as ProfilePage };
