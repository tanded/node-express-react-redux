import { userConstants } from '../actions/types/index';

const { LOGIN_SUCCESS } = userConstants;

const initialState = {
	isAuthenticated: false,
	user: {}
};
export default function auth(state = initialState, action = {}) {
	switch (action.type) {
		case LOGIN_SUCCESS:
			return {
				isAuthenticated: true,
				user: action.user
			};
		default:
			return state;
	}
}
