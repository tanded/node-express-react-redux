import axios from 'axios';
import { setAuthToken } from '../../utils/';
import { userConstants } from '../actions/types/';

const { LOGIN_SUCCESS } = userConstants;

export const setCurrentUser = (user) => {
	return {
		type: LOGIN_SUCCESS,
		user
	};
};

export const getByToken = (token) => {
	return (dispatch) => {
		return axios
			.post('/api/user/data', {
				token: token
			})
			.then(({ data }) => {
				setAuthToken(localStorage.token);
				const { _id, email, name, date } = data;
				const user = {
					_id,
					email,
					name,
					date
				};
				dispatch(setCurrentUser(user));
			})
			.catch((error) => {
				console.log(error);
			});
	};
};

export const login = (credits) => {
	return (dispatch) => {
		return axios
			.post('/api/user/login', {
				email: credits.email,
				password: credits.password
			})
			.then(({ data }) => {
				const { user, token } = data;

				localStorage.setItem('token', token);
				setAuthToken(localStorage.token);
				dispatch(setCurrentUser(user));
			})
			.catch((error) => {
				console.log(error);
			});
	};
};
