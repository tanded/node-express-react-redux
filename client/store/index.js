import { createStore, compose, applyMiddleware } from 'redux';
import authReducer from './reducers/auth.reducer';
import thunk from 'redux-thunk';

export const store = createStore(
	authReducer,
	compose(applyMiddleware(thunk), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
);
